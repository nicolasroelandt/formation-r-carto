Ce dépôt contient les supports pour une introduction
à la manipulation de données géospatiales et à la 
cartographie avec le [langage R](https://cran.r-project.org/).

Ce module est prévu pour une journée et comprend:

- 1 présentation rappelant les bases de l'information géographique
- 1 document Rmarkdown mettant en oeuvre quelques outils de manipulation et de cartographie

Une connaissance des bases du langage R et du [Tidyverse](https://www.tidyverse.org/)
est requise.

Les documents présents dans ce dépôt sont sous licence 
[CC BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/) 
ou selon leur licence originale pour les données, illustrations et bibliothèques logicielles.

- Données source : [Abris vélos de Nantes Métropole ](https://data.nantesmetropole.fr/explore/dataset/244400404_abris-velos-nantes-metropole/export/?disjunctive.commune&disjunctive.conditions&disjunctive.gestionnaire&location=12,47.19508,-1.55405&basemap=jawg.streets)

Ce module a été réalisé par Nicolas Roelandt ([Université Gustave Eiffel](https://www.univ-gustave-eiffel.fr/))